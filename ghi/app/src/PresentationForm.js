import React, { Component } from 'react'

export default class PresentationForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
            conferences: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ conferences: data.conferences });
        }
    }

    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {};
        changeDict[key] = value;
        this.setState(changeDict)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data)
        delete data.conferences;
        const conferenceId = parseInt(data.conference);


        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        if (presentationResponse.ok) {
            const newPresentation = await presentationResponse.json();
            console.log(newPresentation);

            this.setState({
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            })
        }
    }
    

  render() {
    return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={this.state.presenter_name} onChange={this.handleChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.presenter_email} onChange={this.handleChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.company_name} onChange={this.handleChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.title} onChange={this.handleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea value={this.state.synopsis} onChange={this.handleChange} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select value={this.state.conference} onChange={this.handleChange} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {this.state.conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    );
  }
}
