import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        const value = event.target.value
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value 
        this.setState(changeDict)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            }
            this.setState(cleared);
        }
    }


    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new conference</h1>
                            <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.starts} onChange={this.handleChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.ends} onChange={this.handleChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" className="form-label">Description</label>
                                <textarea value={this.state.description} onChange={this.handleChange} className="form-control" name="description" id="description" rows="3"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.max_presentations} onChange={this.handleChange} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                                <label htmlFor="max_presentations">Maximum Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.max_attendees} onChange={this.handleChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                                <label htmlFor="max_attendees">Maximum Attendees</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.location} onChange={this.handleChange} name="location" required id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ConferenceForm;