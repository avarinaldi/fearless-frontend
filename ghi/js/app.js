function createCard(name, description, pictureUrl, startDate, endDate, place) {
  return `
      <div class="card" style="border:0; margins:10px ">
        <div class="shadow p-3 mb-5 bg-body rounded">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${place}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer bg-transparent border-success">${startDate} - ${endDate}</div>
        </div>

      </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
        console.error(e)
    } else {
      const data = await response.json();

      let columnNum = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const place = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = details.conference.starts;
          let startDate = new Date(starts);
          startDate = startDate.toLocaleDateString();
          const ends = details.conference.ends;
          let endDate = new Date(ends);
          endDate = endDate.toLocaleDateString();
          const html = createCard(name, description, pictureUrl, startDate, endDate, place);
          const columns = document.querySelectorAll('.col');
          let column = columns[columnNum];
          columnNum += 1;
          if (columnNum > 2) {
            columnNum = 0;
          }
          column.innerHTML += html;
        }
      }

    }
  } catch (e) {
      console.error(e);
  }

});